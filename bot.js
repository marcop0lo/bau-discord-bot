const Discord = require("discord.js");
const config = require("./config.json");
const client = new Discord.Client();

client.on("ready", readyDiscord);

function readyDiscord() {
  console.log("ready");
}

client.on("message", gotMessage);

function gotMessage(msg) {
  console.log(msg);
  if (msg.content === "/baustil") {
    msg.reply("Carguen Baustil @here");
  }
}

client.login(config.BOT_TOKEN);
